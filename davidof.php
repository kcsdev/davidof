{process_pagedata}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
<head>
<title>{sitename} - {title}</title>
{metadata}
{stylesheet}
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<meta charset="UTF-8">  

</head>
<body>

<!-- start header -->

<!-- end header -->

<!-- start content -->
 
<div class="FormText">
    <div class = "page-header">   
     <h1> מסלקה פנסיונית</h1>  
    </div>
    <div class="text_formt"> 
        <p style="text-align: right;">
            <span>
                <span class="bold">לקוח יקר,</span>
            </span>
        </p>
        <p>
            <span ><br></span>
        </p>
        <p >
            <span>הכנה לטרום פגישת יעוץ עם נציג דוידוף הסדרים פנסיונים.</span>
        </p>
        <p > 
            <span >בהתאם לחוזר משרד האוצר לעניין מסמך הנמקה מיום 23.06.2015 , עלייך לבצע הליך אימות וזיהוי אל מול המסלקה באמצעות כרטיס אשראי 
                <span class="bold">על שמך, </span>
                הליך זה הינו מקוצר ובסיומו דוידוף הסדרים פנסיונים תקבל ישירות את קוד הזיהוי.&nbsp;<br>
                <span class="importantmassege">הבהרה: הרישום באתר אינו כרוך בתשלום, כרטיס האשראי הנו לצורך זיהוי בלבד!<br> (לא ניתן לאמת זיהוי באמצעות כרטיס דיינרס)</span>
                <br>
                <br>
                <br>*אם אין ברשותך כרטיס אשראי, ניתן לבצע הליך אימות וזיהוי ישירות באתר המסלקה והקוד יתקבל בדואר רשום לכתובתך האישית המעודכנת במשרד הפנים. 
                <br>
                <br>
                <span class="bold">לתשומת לבך,</span>
            </span>
        </p>
        <p>
            <span>הליך זה הינו לשרותך והוא עשוי להפתיע אותך לטובה, כל זאת מכוון שלעיתים ישנם כספים הרשומים על שימך 
                <span class="underline">ואינך מודע</span>
                להם. המסלקה מאתרת כל סכום פנסיוני שהופקד עבורך בעבר ומגישה עבורך את המידע החשוב הזה.
            </span>
        </p>
        <p>
            <span>
                <br>פניה על ידינו למסלקה מותנת בכך שהנך רשום במאגר הסוכנות ו/או בכפוף לכך שנתנה הודעת מעסיק למשרדנו אודות הצטרפותך.<br>במידה ואינך בטוח האם הנך רשום במאגר הסוכנות יש למלא פרטי צור קשר במסך.
            </span>
            <br>
        </p>
        <p>
        <span class="inoticed">
            <br>ידוע לי כי בסיום תהליך הרישום, דוידוף הסדרים פנסיונים תקבל קוד אימות, באמצעותו תוכל לפנות למסלקה הפנסיונית ותוכל לקבל מידע אודות התוכניות הפנסיונית מקופות הגמל, קרנות השתלמות, חברות הביטוח וקרנות הפנסיה המנוהלות על שמך, לרבות צבירות, סכומי ביטוח וכל מידע אחר כמפורט בחוזר ממשק להעברת נתונים <br>מבנה אחיד.
        </span>
        </p>
        <p style="text-align: right;">
            <span style="font-size: 16px;">
                <br data-mce-bogus="1">
            </span>
        </p> 
    </div> 
    </div>
    <div class="formsend-wrapper"  >
<!-- action ="https://login.swiftness.co.il/he-IL/saver#/StartWizard/ExternalAuthorizationCode/513802058"  -->
        <form class="formsend" method="get" >
            <div class="form-group">
                    <label for="FirstName">שם פרטי</label>
                    <span class="mandatoryfield">*</span>
                <input type ="text"  class="form-control" id="FirstName" name="FirstName" required>
                <span class="error"></span>
            </div>
             <div class="form-group">
                  <label for="LastName">שם משפחה</label>
                    <span class="mandatoryfield">*</span>

                <input type ="text"  id="LastName" class="form-control"  name="LastName" required>
             <span class="error2"></span>
            </div> 
             <div class="form-group">
                    <label for="IdentificationNumber">ת.ז. [חובה 9 ספרות]</label>
                    <span class="mandatoryfield">*</span>  
                <input type ="text"  maxlength ="9" class ="form-control" id="IdentificationNumber"  name="IdentificationNumber" required>
 <span class="error3"></span>             
   <input type="hidden"  id="AgentIdentificationNumber" name="AgentIdentificationNumber" value="513802058">
            </div>
            <div class="form-group " id="submitter">
                <input class="submitInput" type="submit" value="אישור">
            </div>
        </form>
    </div>
{literal}
     <script>
         jQuery(document).ready(function($) {




    $('#submitter .submitInput').click(function(e) { 
            // Preventing the default submit event
             e.preventDefault();
            // Regular expressions for FrontEnd validation
            var rxName = /[א-ת]/g;
            var rxIDnumber = /([0-9]\d{8})/g;
        
            // Gathering the values of the inputs
            var fname = $("#FirstName").val();
            var lname = $("#LastName").val();
            var IDnumber = $('#IdentificationNumber').val();
            
            var countValidation = 0;
            // Validations
               // First name Validation
            if(fname == "" || !rxName.test(fname)){ 
               $("#FirstName").css("box-shadow","0 0 3px 1px red");
            }else{ 
               $("#FirstName").css("box-shadow","0 0 3px 1px green");
               countValidation++;
            }
               // Last Name Validation
             if(lname == "" || !rxName.test(lname)){ 
               $("#LastName").css("box-shadow","0 0 3px 1px red");
            }else{ 
               $("#LastName").css("box-shadow","0 0 3px 1px green");
               countValidation++;
            }

               // ID validation
            if(IDnumber == "" || !rxIDnumber.test(IDnumber)){ 
               $("#IdentificationNumber").css("box-shadow","0 0 3px 1px red");
            }else{ 
               $("#IdentificationNumber").css("box-shadow","0 0 3px 1px green");
               countValidation++;
            }

            if(countValidation === 3){
           
       

                  var p1 =  jQuery('#FirstName').val();
                  var p2 =  jQuery('#LastName').val();
                  var p3 =  jQuery('#IdentificationNumber').val();
                  var p4 =  jQuery('#AgentIdentificationNumber').val();
                  var url = 'https://login.swiftness.co.il/he-IL/saver#/StartWizard/ExternalAuthorizationCode/'+p4+'&'+p3+'&'+p1+'&'+p2;

                   window.location.href = url;
   
           }
  
   
  });
});
     </script>
{/literal}
</body>
</html>
